package sda3.amen.com.simplefragment;


import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import sda3.amen.com.simplefragment.interfaces.IDatabaseInstanceProvider;
import sda3.amen.com.simplefragment.model.PRODUCT_TYPE;
import sda3.amen.com.simplefragment.model.Product;


/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment extends Fragment {

    // potrzebne by mieć dostęp do (providera) bazy danych
    private IDatabaseInstanceProvider dbProvider;

    private ArrayAdapter<String> adapter;

    @BindView(R.id.editName)
    protected EditText editName;

    @BindView(R.id.editPrice)
    protected EditText editPrice;

    @BindView(R.id.spinner)
    protected Spinner spinner;

    @OnClick(R.id.button)
    protected void saveProduct() {
        Product p = new Product(editName.getText().toString(),
                Double.valueOf(editPrice.getText().toString()),
                PRODUCT_TYPE.valueOf(spinner.getSelectedItem().toString()));

        cleanView();

        DataProvider.INSTANCE.addProduct(p);
        dbProvider.getDatabaseProviderInstance().addProduct(p);
    }

    /**
     * Ustawiamy db providera - tak jak w SecondFragment.
     * Jest nam potrzebny, ponieważ pole bazy danych jest
     * dostępne wyłącznie w MainActivity (które implementuje
     * IDatabaseInstanceProvider).
     * @param db - provider (MainActivity)
     */
    public void setDBProvider(IDatabaseInstanceProvider db) {
        this.dbProvider = db;
    }

    private void cleanView() {
        editName.getText().clear();
        editPrice.getText().clear();
        spinner.setSelection(0);

        editName.requestFocus();

        // hide keyboard
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    public BlankFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_blank, container, false);

        // podbindowanie butterknife - drugi parametr to widok.
        ButterKnife.bind(this, v);

        // ustawienie wartości spinnera
        adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.product_types));
        spinner.setAdapter(adapter);

        // zwrócenie widoku
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getArguments() != null) {
            int position = getArguments().getInt("ItemPosition");
            Product p = DataProvider.INSTANCE.getProducts().get(position);

            editName.setText(p.getName());
            editPrice.setText(String.valueOf(p.getPrice()));
            // TODO: ustawić spinner (wskazówka: adapter musi być polem a
            // TODO: nie zmienną tymczasową w metodzie onCreateView

            spinner.setSelection(adapter.getPosition(p.getType().toString()));
        }
    }
}
