package sda3.amen.com.simplefragment.interfaces;

import sda3.amen.com.simplefragment.DatabaseProvider;
import sda3.amen.com.simplefragment.model.Product;

/**
 * Created by amen on 6/6/17.
 */

/**
 * Provider który został zaimplementowany w klasie MainActivity.
 * Celem stworzenia tego interfejsu jest dostarczenie do
 * fragmentów dostępu do pola z MainActivity, ale zablokowanie
 * wywołania pozostałych metod - jeśli przekażemy obiekt interfejsu
 * to w klasie fragmentu postrzegamy MainActivity jako IDatabaseInstanceProvider
 * który posiada wyłącznie jedną metodę (poniższą).
 */
public interface IDatabaseInstanceProvider {
    DatabaseProvider getDatabaseProviderInstance();
}
