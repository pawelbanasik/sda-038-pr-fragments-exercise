package sda3.amen.com.simplefragment;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.LinkedList;
import java.util.List;

import sda3.amen.com.simplefragment.model.PRODUCT_TYPE;
import sda3.amen.com.simplefragment.model.Product;

/**
 * Created by amen on 6/5/17.
 */

public class DatabaseProvider extends SQLiteOpenHelper {

    private List<Product> list;

    // stałe wersji bazy oraz nazwy bazy
    private static final String DB_NAME = "products.db";
    private static final int DB_VERSION = 1;

    // stałe - nazwy kolumn i tabeli
    private static final String DB_TABLE_NAME = "products";

    // kolumny
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_PRICE = "price";
    private static final String COLUMN_TYPE = "type";

    public DatabaseProvider(Context context) {
        super(context, DB_NAME, null, DB_VERSION);

        list = new LinkedList<>();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DB_TABLE_NAME + " ( " +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_NAME + " VARCHAR, " +
                COLUMN_PRICE + " REAL," +
                COLUMN_TYPE + " VARCHAR );"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE_NAME);
        onCreate(db);
    }

    // insert into DB_NAME ( KOLUMNY) VALUES (WARTOSCI);
    private void insertRecordIntoDatabase(Product product) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(COLUMN_ID, product.getId());
        cv.put(COLUMN_NAME, product.getName());
        cv.put(COLUMN_PRICE, product.getPrice());
        cv.put(COLUMN_TYPE, product.getType().toString());

        db.insert(DB_TABLE_NAME, null, cv);
    }

    public void load() {
        list.clear();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DB_TABLE_NAME, null);

        for (int i = 0; i < cursor.getCount(); i++) {
            cursor.moveToNext();

            // pobieram 3 wartosci
            int id = cursor.getInt(0);
            String name = cursor.getString(1);
            double price = cursor.getDouble(2);
            String type = cursor.getString(3);

            // tworze obiekt, ustawiam 3 wartości
            Product newProduct = new Product(id, name, price, PRODUCT_TYPE.valueOf(type));

            // dodaje do listy
            list.add(newProduct);
        }
    }

    public void save() {
        for (Product p : list) {
            if (p.getId() == -1) {
                insertRecordIntoDatabase(p);
            }
        }
    }

    public List<Product> getList() {
        return list;
    }

    public void addProduct(Product p) {
        // równie dobrze można dodać do bazy.
        list.add(p);
    }
}