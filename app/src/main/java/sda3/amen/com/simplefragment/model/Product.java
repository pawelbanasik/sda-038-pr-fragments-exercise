package sda3.amen.com.simplefragment.model;

/**
 * Created by amen on 6/3/17.
 */

public class Product {
    private int id =-1;
    private String name;
    private Double price;
    private PRODUCT_TYPE type;
    // 519 088 399
    public Product(int id, String name, Double price, PRODUCT_TYPE type) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.type = type;
    }

    public Product(String name, Double price, PRODUCT_TYPE type) {
        this.name = name;
        this.price = price;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public PRODUCT_TYPE getType() {
        return type;
    }

    public void setType(PRODUCT_TYPE type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", type=" + type +
                '}';
    }
}
